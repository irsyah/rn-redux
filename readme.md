# TUGAS

https://imdb-api.com/

Buatlah Server dan Client-nya

- Buatlah Login dan Registrasi
- Saat berhasil Login, Buatlah Menu (Silakan pilih Tab atau Drawer)
    - Menu Home
        - List semua Movie yang inTheaters
    - Menu Search Movie
        - Tampilkan list movie berdasarkan hasil Search
        - Double tap untuk memasukkan ke Watchlist dan data disimpan ke server Java kalian
            - berikan authentication untuk api server kamu!
            - info column tabel pada database kamu:
                - id
                - image
    - Menu Watchlist
        - fetch data yang ada di server movie/watchlist kamu, yang ditampilkan adalah image-image dari list watchlist movie
        - saat movie di tap satu kali, maka akan diarahkan ke halaman Movie detail yang menampilkan data
            - Title
            - Image
            - Description
            - Released Date
            - Duration film
            - Directors
            - Stars/Cast
            - Genre
            - imdb rating
            - Tombol untuk melihat kumpulan Reviews => ke halaman Reviews dan tampilkan list review untuk movie tersebut (silakan baca dokumentasi API untuk cara mendapatkan review movie)
        


