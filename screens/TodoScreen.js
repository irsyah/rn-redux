import { View, Text, ScrollView, SafeAreaView, Alert } from 'react-native'
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useEffect } from 'react';
import { fetchTodos } from '../redux/actions/todoAction';

export default function TodoScreen() {
  //cara memanggil state yang ada di store 
  const todos = useSelector(state => state.todos);

  const dispatch = useDispatch();

//   const fetchTodos = async () => {
//     try {
//         const response = await fetch("https://jsonplaceholder.typicode.com/todos");
//         if (response.ok) {
//             const data = await response.json();
//             dispatch(todoFetch(data));
//         } else {
//             throw new Error("Something went wrong")
//         }
//     } catch (err) {
//         Alert.alert(err)
//     }
//   }

  useEffect(() => {
    dispatch(fetchTodos());
  }, []);

  return (
    <SafeAreaView>
      <ScrollView>
        { todos.map((todo, index) => (
            <Text key={todo.id}>{todo.title}</Text>
        ))}
      </ScrollView>
    </SafeAreaView>
  )
}