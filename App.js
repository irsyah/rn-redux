import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import store from './redux/store';
import TodoScreen from './screens/TodoScreen';

export default function App() {
  return (
    <Provider store={store}>
      <TodoScreen />
    </Provider>
  );
}


// harus install package redux dan react-redux