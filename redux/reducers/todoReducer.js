
// reducer function biasa, menerima 2 parameter
// harus selalu me-return state-nya
const todoReducer = (state = [], action) => {
    switch (action.type) {
        case "FETCH_TODO":
            return action.payload // new state ['hello']
            break;
        case "DELETE_TODO":
            return state.filter(data => {
                if (data.id !== action.payload.id) {
                    return data
                }
            })
            break;
        default:
            return state; // current state
            break;
    }
}

export default todoReducer;