export const todoFetch = (payload) => {
    return { type: "FETCH_TODO", payload }
}

export const todoRemove = (id) => {
    return { type: "DELETE_TODO", payload: id}
}

// function middleware 
export const fetchTodos = () => {

    //interupt proses sebelum masuk ke reducer
    //middleware redux-thunk
    return async function(dispatch, getState) {

        console.log("-----------");
        console.log(getState());
        try {
            const response = await fetch("https://jsonplaceholder.typicode.com/todos");
            if (response.ok) {
                const data = await response.json();
                dispatch(todoFetch(data));
                // dispatch(middleware2());
            } else {
                throw new Error("Something went wrong")
            }
        } catch (err) {
            console.log(err);
        }
    }
}

const middleware2 = () => {
    return function(dispatch) {
        // logika proses bisnisnya
        dispatch(actionnya)
    }
}